//
//  GalleryView.m
//  PlantView
//
//  Created by kimsung jun on 2014. 4. 8..
//  Copyright (c) 2014년 kimsung jun. All rights reserved.
//

#import "GalleryView.h"
#import "AppDelegate.h"
#import "PictureView.h"
#import "PictureView2.h"


@interface GalleryView (){
    
    AppDelegate *delegate;
    
    CGRect btn_frame;
    
    
    
    NSMutableArray *myArray;
    
    int obj_cnt;
    
    int obj_dv;
    
    NSDictionary *dic;
    
    UIImage *image;
    
    UIImage *image2;
    
    PictureView *pView;
    
    PictureView2 *pView2;
    
}

@end

@implementation GalleryView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    pView = [[PictureView alloc]initWithNibName:@"PictureView" bundle:nil];
    pView2 = [[PictureView2 alloc]initWithNibName:@"PictureView2" bundle:nil];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    myArray = [NSMutableArray array];
    
    [myArray removeAllObjects];
    
    [myArray setArray:[delegate getRecotdsWithTableName:delegate.now_Title]];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        
        obj_cnt = (int)[myArray count];
        
        //    obj_cnt = delegate.img_Cnt;
        
        if (obj_cnt != 0) {
            
            UIButton *btn[obj_cnt+1];
            
            
            if (obj_cnt%3 == 0) {
                obj_dv = obj_cnt/3;
            }else{
                obj_dv = obj_cnt/3+1;
            }
            
            
            btn_frame = CGRectMake(20, 70, 80, 100);
            
            int a[obj_dv][3];
            
            int k=0;
            
            int cnt2 = 0;
            for (int i=0; i<obj_dv ; i++) {
                
                for (int j=0; j<3; j++) {
                    a[i][j] = cnt2;
                    
                    cnt2++;
                    
                    if (cnt2 == obj_cnt){
                        break;
                    }
                }
            }
            int cnt =0;
            
            for (int i=0; i<obj_dv; i++) {
                
                for (int j=0; j<3; j++) {
                    
                    if (a[i][j] == cnt) {
                        dic = myArray[cnt];
                        
                        btn[cnt] = [[UIButton alloc]initWithFrame:CGRectMake(btn_frame.origin.x+(j*100) , btn_frame.origin.y+(i*120) , 80, 100)];
                        
                        btn[cnt].tag = cnt;
                        
                        image = [UIImage imageWithData:[dic objectForKey:@"img2"]];
                        
                        
                        
                        [btn[cnt] setImage:image forState:UIControlStateNormal];
                        
                        [btn[cnt] addTarget:self action:@selector(action_Btn:) forControlEvents:UIControlEventTouchDown];
                        
                        [self.view setNeedsDisplay];
                        
                        [_scrollView addSubview:btn[cnt]];
                        
                        if (k<3) {
                            k++;
                        }else{
                            k=0;
                        }
                        cnt++;
                    }
                    if (cnt == obj_cnt+1) {
                        break;
                    }
                }
            }
            
            
            _scrollView.scrollEnabled = YES;
            
            if (obj_dv > 3) {
                
                [_scrollView setContentSize:CGSizeMake(320, 480+(obj_dv/3*150))];
                
            }else{
                [_scrollView setContentSize:CGSizeMake(320, 480)];
            }
            
        }
        
    }else{
        
        obj_cnt = (int)[myArray count];
        
        //    obj_cnt = delegate.img_Cnt;
        
        if (obj_cnt != 0) {
            
            UIButton *btn[obj_cnt+1];
            
            
            if (obj_cnt%5 == 0) {
                obj_dv = obj_cnt/5;
            }else{
                obj_dv = obj_cnt/5+1;
            }
            
            
            
            btn_frame = CGRectMake(20, 40, 160, 200);
            
            int a[obj_dv][5];
            
            int k=0;
            
            int cnt2 = 0;
            
            NSLog(@"obj dv = %d",obj_dv);
            for (int i=0; i<obj_dv ; i++) {
                
                for (int j=0; j<5; j++) {
                    a[i][j] = cnt2;
                    
                    cnt2++;
                    
                    if (cnt2 == obj_cnt){
                        break;
                    }
                }
                
                NSLog(@" i = %d", i);
            }
            int cnt =0;
            
            for (int i=0; i<obj_dv; i++) {
                
                for (int j=0; j<5; j++) {
                    
                    if (a[i][j] == cnt) {
                        dic = myArray[cnt];
                        
                        btn[cnt] = [[UIButton alloc]initWithFrame:CGRectMake(btn_frame.origin.x+(j*150) , btn_frame.origin.y+(i*180) , 160, 200)];
                        
                        btn[cnt].tag = cnt;
                        
                        image = [UIImage imageWithData:[dic objectForKey:@"img2"]];
                        
                        
                        
                        [btn[cnt] setImage:image forState:UIControlStateNormal];
                        
                        [btn[cnt] addTarget:self action:@selector(action_Btn:) forControlEvents:UIControlEventTouchDown];
                        
                        [self.view setNeedsDisplay];
                        
                        [_scrollView addSubview:btn[cnt]];
                        
                        if (k<5) {
                            k++;
                        }else{
                            k=0;
                        }
                        cnt++;
                    }
                    if (cnt == obj_cnt+1) {
                        break;
                    }
                }
            }
            
            
            _scrollView.scrollEnabled = YES;
            
            if (obj_dv > 5) {
                
                [_scrollView setContentSize:CGSizeMake(768, 1024+(obj_dv/5*50))];
                
            }else{
                [_scrollView setContentSize:CGSizeMake(768, 1024)];
            }
            
        }
        
    }
    
    

}

-(void)viewDidDisappear:(BOOL)animated{
    
  
}

-(void)action_Btn:(UIButton *)sender{
    
    [self sound_btn:@"click1"];
    
   int num = (int)sender.tag;
    
    
    NSLog(@"tag = %d", num);
//
    dic = myArray[num];
    
    image2 = [UIImage imageWithData:[dic objectForKey:@"img1"]];
    
   
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        
         pView2.image = image2;
        
        [self.navigationController pushViewController:pView2 animated:YES];
        
    }else{
        
         pView.image = image2;
        
        [self.navigationController pushViewController:pView animated:YES];
    }
    
    
    
}

-(void)makeBtn:(CGRect *)rect{
    
//    btn = [[UIButton alloc]initWithFrame:btn_frame];
//    
//    btn.backgroundColor = [UIColor redColor];
//    
//    [self.view addSubview:btn];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) sound_btn:(NSString *) sound_name{
    
    NSString *str = sound_name;
    
    NSError *error;
    
    NSString *path = [[NSBundle mainBundle]pathForResource:str ofType:@"mp3"];
    
    NSURL *url = [NSURL fileURLWithPath:path];
    
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    
    self.player.volume = 0.5;
    
    [self.player prepareToPlay];
    
    [self.player play];
}


@end
